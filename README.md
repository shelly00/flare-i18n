# flare-i18n

⚡ Realtime internationalization service.

## Translation manager app internal

### `subscribe-translation :: `[`CollectionReference`](https://firebase.google.com/docs/reference/js/firebase.firestore.CollectionReference)` -> language -> (`[`QuerySnapshot`](//firebase.google.com/docs/reference/js/firebase.firestore.QuerySnapshot)` -> IO ()) -> IO()`

Subscribe to translation data matches specified `language` in the firestore collection. Return a function that unsubscribes the change listener.

### `extract-translation :: QuerySnapshot -> translation`

Extract translation data from a firestore `QuerySnapshot`, data are merged if the `QuerySnapshot` has multiple documents.

#### Example result

```js
{
  dessert: 'お菓子'
}
```

### `add-resource :: i18next -> translation -> IO ()`

Add translation data to the i18next instance.

### State shape

```js
{
  auth: {
    uid: '...'
  },
  languages: ['ja'],
  data: {
    ja: {
      dessert: 'お菓子'
    }
  },
  newEntry: {
    key: 'sugar',
    ja: '砂糖'
  }
}
```

### `load-snapshot :: snapshot -> action`

Wrap translation data into an action that load it into UI state.

```js
{
  type: 'load-snapshot',
  payload: [
    ja: {
      dessert: 'お菓子'
    }  
  ]
}
```

### `add-translation :: {target} -> IO ()`

Get translation text from the form submit event add them to firebase.

### `update-translation :: {language, data} -> IO ()`

Add or update specified translation text entries. Create a new document if specified `language` is missing.

## Vue Plugin

Provides translating function `t()` and subscribe components to **real-time** translation data changes.

### Examples

```js
import install form 'flare-i18n/vue'

firebase.initializeApp({
  apiKey: '### FIREBASE API KEY ###',
  authDomain: '### FIREBASE AUTH DOMAIN ###',
  projectId: '### CLOUD FIRESTORE PROJECT ID ###'
})


i18next.init({ lng: 'en' }, start)

function start(err, t) {
  Vue.use({ install }, {
    i18next,
    collection: firebase.firestore().collection('i18n')
  })  
}
```
