sample-data = dessert: \お菓子

function pack-snapshot
  data = Object.assign $language: \ja $segment: \common, it
  docs: [data: -> data]

function mock-collection
  test = {}

  test: test, where: (field, op, value) ->
    test.query = [field, op, value]join ' '
    get: -> then: (next) ->
      next docs: [ref: update: -> test.update = it]
    on-snapshot: (next) ->
      test.on-change = next
      test.emit = -> next pack-snapshot it
      next pack-snapshot sample-data
      test.unsubscribe = -> test.on-change := void

function mock-firebase
  test: test = {}
  firestore: -> collection: -> test.collection = mock-collection!

function mock-submit
  global.FormData = (form) !->
    @entries = -> form
  prevent-default: ->
  target: [[\key \sugar] [\ja \砂糖]]

function mock-change value
  target: {value}

export {mock-firebase, mock-collection, mock-submit, mock-change, sample-data}
