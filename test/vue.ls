import
  \../src/vue : install
  \./mock : {mock-collection, sample-data}

function mock-i18next
  language = \ja-JP
  data = {}
  test = {}

  test: test, language: language
  add-resource-bundle: (language, namespace, resources, deep, overwrite) ->
    test.bundle = {language, namespace, resources, deep, overwrite}
    data[language] = Object.assign {} data[language], resources
  t: (key) -> data[language][key]

function mock-vue
  mixins = {}
  Object.assign (->),
    create: ->
      instance =
        $force-update: ~> instance.updated = true
        $destroy: ~> mixins.before-destroy.call instance
      mixins.before-mount.call instance
      instance
    mixin: -> Object.assign mixins, it

function main t
  Vue = mock-vue!
  collection = mock-collection!
  i18next = mock-i18next!
  install Vue, {i18next, collection}

  actual = collection.test.query
  expected = '$language == ja-JP'
  t.is actual, expected, 'query with current language'

  actual = i18next.test.bundle
  expected =
    language: \ja-JP namespace: \translation
    resources: Object.assign $language: \ja $segment: \common, sample-data
    deep: true overwrite: true
  t.same actual, expected, 'add initial translation bundle to i18next'

  actual = Vue::t \dessert
  expected = \お菓子
  t.is actual, expected, 'provide a translating function'

  m0 = Vue.create!
  m1 = Vue.create!
  m1.$destroy!
  collection.test.emit sugar: \砂糖

  actual = Vue::t \sugar
  expected = \砂糖
  t.is actual, expected, 'load additional translation entries'

  actual = m1.updated
  t.false actual, 'unsubscribe destroyed components'

  actual = m0.updated
  t.true actual, 'force update subscribed components'

  t.end!

export default: main
