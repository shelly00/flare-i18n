import \../src/list : {list-state, list-props}
import \./mock : {mock-firebase, mock-submit, mock-change}

function main t
  state = data: ja: tea: \お茶

  actual = list-state state
  expected = state.data
  t.is actual, expected, 'required state part for list'

  {items: [entry]} = props = list-props state.data
  actual = entry.key
  row = key: \tea values: [{value: \お茶}]
  expected = \tea
  t.is actual, expected, 'list component entry key'

  actual = entry.values.0.value
  expected = \お茶
  t.is actual, expected, 'list component entry content'

  global.firebase = mock-firebase!

  entry.values.0.save mock-change \t
  actual = global.firebase.test.collection.test.update
  expected = tea: \t
  t.same actual, expected, 'list component entry save'

  actual = props.languages
  expected = [\ja]
  t.same actual, expected, 'list component languages'

  props.add mock-submit!

  actual = global.firebase.test.collection.test.update
  expected = sugar: \砂糖
  t.same actual, expected, 'list component add'

  global.firebase = void

  t.end!

export default: main
