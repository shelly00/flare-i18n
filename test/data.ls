import
  \../src/data : {
    reduce, subscribe-translation, update-translation, setup-store
  }
  \./mock : {mock-firebase, mock-collection, sample-data}

function main t
  collection = mock-collection!
  snapshot = void
  on-change = -> snapshot := it

  actual = subscribe-translation collection, \ja on-change
  expected = collection.test.unsubscribe
  t.is actual, expected, 'return a function to unsubscribe'

  actual = collection.test.query
  expected = '$language == ja'
  t.is actual, expected, 'query with specified language'

  actual = snapshot.docs.0.data!dessert
  expected = \お菓子
  t.is actual, expected, 'pass new snapshot to change listeners'

  global.firebase = mock-firebase!
  options =
    language: \ja
    data: sugar: \砂糖
  update-translation options
  {collection} = global.firebase.test

  actual = collection.test.query
  expected = '$language == ja'
  t.is actual, expected, 'find target translation document to update'

  actual = collection.test.update
  expected = options.data
  t.is actual, expected, 'update target translation document'

  global.firebase = void

  state = ja: dessert: \お菓子

  actual = reduce\translation-snapshot state, [ja: sugar: \砂糖] .ja
  expected = sugar: \砂糖
  t.same actual, expected, 'merge loaded snapshot'

  t.end!

export default: main
