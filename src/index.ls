import
  \./link : {start-app}
  \../style/index : styles
  \./reduce : reducers
  \./preload : preload
  \./main : main
  \./setup : setup

function start
  store = start-app {reducers, preload, component: main}
  setup store
  navigator.service-worker?register \/sw.js
  if module.hot
    require \preact/devtools
    module.hot.accept \./reduce -> store.replace-reducer reducers
    module.hot.accept \./main -> store.replace-app main

start!
