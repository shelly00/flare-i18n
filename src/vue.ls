import \./data : {subscribe-translation, extract-translation, add-resource}

function setup collection, i18next, next
  subscribe-translation collection, i18next.language, ->
    add-resource i18next, extract-translation it
    next!

function install Vue, {i18next, collection}
  listeners = new Set
  setup collection, i18next, ->
    Array.from listeners.keys! .for-each -> it.$force-update!

  Vue::t = -> i18next.t it
  Vue.mixin do
    before-mount: -> listeners.add @
    before-destroy: -> listeners.delete @

export default: install
