import
  \../firebase.app.json : app-options

external-list =
  'https://www.gstatic.com/firebasejs/4.5.0/firebase.js'
  'https://www.gstatic.com/firebasejs/4.5.0/firebase-firestore.js'

function load-external items
  [Promise.resolve!]concat items .reduce (last, src) ->
    element = document.create-element \script
    last.then -> new Promise (resolve) ->
      document.body.append Object.assign element, {onload: resolve, src}

function extract-entries
  it.docs.map ->
    {$language, ...rest} = it.data!
    ($language): rest

function access-log
  if it && it.uid
    firebase.firestore!collection \users .doc that
    .set accessed-at: true, {merge: true}

function setup store
  load-external external-list .then ->
    firebase.initialize-app app-options
    firebase.auth!on-auth-state-changed ->
      store.dispatch type: \auth payload: it
      access-log it
    firebase.firestore!collection \i18n .on-snapshot ->
      load = type: \translation-snapshot payload: extract-entries it
      store.dispatch load

export default: setup
