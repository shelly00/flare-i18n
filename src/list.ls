import
  \./link : {link}
  \./list-block : list-block
  \./data : {update-translation}

function list-state state
  state.data

function add-translation {target}: event
  event.prevent-default!
  data = new FormData target
  items = Array.from data.entries!
  key = items.find (.0 == \key) .1
  items.filter (.0 != \key) .for-each ([language, text]) ->
    update-translation {language, data: (key): text}

function list-props state
  languages = Object.keys state
  languages: languages.slice 0 1
  items: Object.entries state[languages.0] || {} .map ([key, translation]) ->
    save = (target: {value}) ->
      update-translation language: languages.0, data: (key): value
    {key, values: [{value: translation, save}]}
  add: add-translation

list = link list-block, list-state, list-props

export {default: list, list-state, list-props}
