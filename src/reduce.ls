import
  \./auth : reduce: auth
  \./data : reduce: data

reducers = {auth, data}

export default: reducers
