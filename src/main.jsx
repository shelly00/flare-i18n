import {h} from './link'
import auth from './auth'
import content from './content'
import list from './list'

export default () =>
<div>
  <auth />
  <list />
  <content />
</div>
