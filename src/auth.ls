import
  \./link : {h, link}
  \./auth-block : auth-block

reduce =
  auth: (, payload) ->
    if payload then {payload.uid} else void

function sign-in
  firebase.auth!sign-in-with-popup new firebase.auth.GoogleAuthProvider

function auth-state => it.auth
function auth-props => if it.uid then {} else {sign-in}
auth = link auth-block, auth-state, auth-props

export {reduce, default: auth}
