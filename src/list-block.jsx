import {h} from './link'

const addTranslation = ({languages, add}) =>
<form onSubmit={add}>
  <div class="row">
    <editable required value="" name="key" placeholder="Add key"/>
    {languages.map(language =>
      <editable required value="" name={language} />
    )}
  </div>
  <input type="submit" hidden />
</form>

const editable = props =>
<div class="editable">
  <input {...props} />
</div>

export default ({languages, items, add}) =>
<div class="list">
  <div class="head">
    <div></div>
    {languages.map(language =>
      <div>{language}</div>
    )}
  </div>
  {items.map(({key, values}) =>
    <div class="row">
      <div>{key}</div>
      {values.map(({value, save}) =>
        <editable value={value} onChange={save}/>
      )}
    </div>
  )}
  <add-translation languages={languages} add={add} />
</div>
