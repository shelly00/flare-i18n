import {h} from './link'

export default ({signIn}) =>
signIn?
  <button onClick={signIn}>Sign In</button>:
  'Signed In'
