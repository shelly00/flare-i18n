function subscribe-translation collection, language, on-change
  collection.where \$language \== language .on-snapshot on-change

function extract-translation
  Object.assign {} ...it.docs.map (.data!)

function add-resource i18next, bundle
  i18next.add-resource-bundle i18next.language, \translation bundle, true true

function update-translation {language, data}
  firebase.firestore!collection \i18n .where \$language \== language
  .get!then -> it.docs.0.ref.update data

function is-object => it && Object:: == Object.get-prototype-of it

function deep-merge base, ...sources => switch
  | sources.length == 0 => base
  | is-object base and is-object sources.0
    diff = Object.entries sources.0 .map ([key, value]) ->
      (key): if key of base then deep-merge base[key], sources.0[key]
      else sources.0[key]
    next = Object.assign {} base, ...diff
    deep-merge next, ...sources.slice 1
  | _ => deep-merge ...sources

reduce =
  \translation-snapshot : (, payload) ->
    deep-merge {} ...payload

export {
  reduce, subscribe-translation, update-translation
  extract-translation, add-resource
}
