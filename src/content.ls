import
  \./link : {h, link}
  \./list : {list-state, list-props}

function content-state => it

function dump => h \pre,, JSON.stringify it,, 2

content = link dump, list-state, list-props

export default: content
